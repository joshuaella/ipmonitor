Ip Monitor
=======
A windows service that monitors your internal and external Ip for changes

### LICENSE
MIT License- see LICENSE

### INFO
  * It will check for Ip Changes and notify you via slack
  * It will only notify you when an Ip changes or on initial start up

### Getting started
1. Download [.Net SDK](https://dotnet.microsoft.com/en-us/download/dotnet/7.0)
2. Build the solution
3. Go to the build directory
4. Copy the content to a path of your choice

### Build and Install

- The below will build and install the service to `${root}/out/IpMonitor` 
- For the service to run it will require that `SYSTEM` has modify Permission on the above directory
- The above is done by the install script

> In windows cmd

``` cmd
: Publish the monitor (From Root) 
cd src\IpMonitor
src\IpMonitor> dotnet publish -c Release --output ../../out/IpMonitor
src\IpMonitor> install.bat
```

#### REQUIREMENTS
To run the build, you will require .net 7.0 and an IDE capable of building.


#### CREDITS
Joshua Ella Ltd - A software consultancy  
