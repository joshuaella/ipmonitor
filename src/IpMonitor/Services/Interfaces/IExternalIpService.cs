namespace IpMonitor.Services.Interfaces;
public interface IExternalIpService
{
    Task Start(CancellationToken stoppingToken);
    void Stop();
}