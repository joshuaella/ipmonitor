﻿using IpMonitor.Models;

namespace IpMonitor.Services.Interfaces;

public interface INotificationService
{
    Task<bool> Notify(AdapterChange adapterChange, CancellationToken token);
}