using Refit;

namespace IpMonitor.Services.Interfaces;
public interface IExternalIpApi
{
    [Get("/")]
    Task<IApiResponse<string>> GetExternalIp(CancellationToken token);
}