using Refit;

namespace IpMonitor.Services.Interfaces;

public interface ISlackApi
{
    [Post("/services/{token1}/{token2}/{token3}")]
    Task<IApiResponse> UpdateSlack([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, string> data, string token1, string token2, string token3, CancellationToken token);

}