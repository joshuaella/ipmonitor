﻿namespace IpMonitor.Services.Interfaces;

public interface ILocalIpService
{
    void Start();
    void Stop();
}