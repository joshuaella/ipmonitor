﻿namespace IpMonitor.Services.Interfaces;

public interface IRestCallService
{
    ISlackApi SlackApi { get; }
    IExternalIpApi ExternalApi { get; }
        
}