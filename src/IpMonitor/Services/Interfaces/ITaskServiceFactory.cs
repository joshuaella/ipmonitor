﻿namespace IpMonitor.Services.Interfaces;

public interface ITaskServiceFactory
{
    Task Delay(TimeSpan time, CancellationToken token);
}