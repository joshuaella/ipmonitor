﻿using IpMonitor.Services.Interfaces;

namespace IpMonitor.Services;

public class TaskServiceFactory : ITaskServiceFactory
{
    public Task Delay(TimeSpan time, CancellationToken token)
    {
        return Task.Delay(time, token);
    }
}