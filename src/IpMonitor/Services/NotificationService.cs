﻿using IpMonitor.Models;
using IpMonitor.Services.Interfaces;

namespace IpMonitor.Services;

public class NotificationService : INotificationService
{
    private readonly string _slackToken1;
    private readonly string _slackToken2;
    private readonly string _slackToken3;
    private readonly IRestCallService _restCallService;
    private readonly ITaskServiceFactory _taskServiceFactory;
    private readonly ILogger<NotificationService> _logger;

    public NotificationService(
        string slackToken,
        IRestCallService restCallService,
        ITaskServiceFactory taskServiceFactory,
        ILogger<NotificationService> logger)
    {
        var slackTokens = slackToken.Split('/');
        _slackToken1 = slackTokens[0];
        _slackToken2 = slackTokens[1];
        _slackToken3 = slackTokens[2];
        _restCallService = restCallService;
        _taskServiceFactory = taskServiceFactory;
        _logger = logger;
    }

    public async Task<bool> Notify(AdapterChange adapterChange, CancellationToken token)
    {
        //payload={"text": "This is a line of text in a channel.\nAnd this is another line of text."}
        var data = new Dictionary<string, string>
        {
            {"payload", $"{{\"text\": \"{adapterChange}\"}}"}
        };

        var retry = 0;
        while (retry < 5)
        {
            retry++;
            if (token.IsCancellationRequested)
            {
                break;
            }

            try
            {
                var response = await _restCallService
                    .SlackApi
                    .UpdateSlack(data, _slackToken1, _slackToken2, _slackToken3, token);

                if (!response.IsSuccessStatusCode)
                {
                    _logger.LogError("Failed to update slack: status: {StatusCode} reason: {ReasonPhrase}",
                        response.StatusCode,
                        response.ReasonPhrase);
                }

                return response.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unhandled Exception. message={Message}", ex.Message);
                if (token.IsCancellationRequested)
                {
                    break;
                }

                await _taskServiceFactory.Delay(TimeSpan.FromMinutes(1), token);
            }

        }

        return false;
    }
}