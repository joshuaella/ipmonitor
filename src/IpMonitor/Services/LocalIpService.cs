﻿using System.Net.NetworkInformation;
using System.Runtime.Versioning;
using IpMonitor.Models;
using IpMonitor.Services.Interfaces;

namespace IpMonitor.Services;

public class LocalIpService : ILocalIpService
{
    private Dictionary<string, List<string>> _ipAddresses;
    private readonly INotificationService _notificationService;
    private readonly ILogger<LocalIpService> _logger;
    private CancellationTokenSource _cancellationTokenSource;

    public LocalIpService(
        INotificationService notificationService,
        ILogger<LocalIpService> logger
    )
    {
        _ipAddresses = new Dictionary<string, List<string>>();
        _notificationService = notificationService;
        _logger = logger;
        _cancellationTokenSource = new CancellationTokenSource();
    }

    public void Start()
    {
        _cancellationTokenSource.Cancel();
        _cancellationTokenSource.Dispose();
        _cancellationTokenSource = new CancellationTokenSource();
        _ipAddresses = new Dictionary<string, List<string>>();
        _logger.LogInformation("Starting Local IP Monitor");
        NetworkChange.NetworkAddressChanged += OnNetworkAddressChanged;
        HandleNetworkChange();
    }

    public void Stop()
    {
        _cancellationTokenSource?.Cancel();
        _cancellationTokenSource?.Dispose();
        NetworkChange.NetworkAddressChanged -= OnNetworkAddressChanged;
        _ipAddresses = new Dictionary<string, List<string>>();
        _logger.LogInformation("Stopping Local IP Monitor");
    }

    private void OnNetworkAddressChanged(object? _, EventArgs e)
    {
        _logger.LogInformation("Local Network Changed");
        HandleNetworkChange();
    }

    [SupportedOSPlatform("windows")]
    private async Task HandleNetworkChange()
    {
        var adapters = new List<string>();
        foreach (var ni in NetworkInterface.GetAllNetworkInterfaces())
        {
            if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
            {
                if (ni.OperationalStatus != OperationalStatus.Up)
                {
                    continue;
                }

                var adapterName = ni.Name;
                var hasAdapter = _ipAddresses.TryGetValue(adapterName, out var ips);
                var ipAddresses = new List<string>();
                adapters.Add(adapterName);
                foreach (var ip in ni.GetIPProperties().UnicastAddresses.Where(a => a.IsDnsEligible))
                {
                    var ipAddress = ip.Address.ToString();
                    _logger.LogInformation($"Adapter Ip change. adapter={adapterName} ip={ipAddress}");
                    ipAddresses.Add(ipAddress);
                }

                if (!hasAdapter)
                {
                    if (!ListsMatch(ips, ipAddresses))
                    {
                        _logger.LogInformation(
                            $"Adding Ip to adapter collection and notifying. adapter={adapterName} ip={string.Join(", ", ipAddresses)}");
                        var result = await _notificationService.Notify(new AdapterChange(adapterName, ipAddresses),
                            _cancellationTokenSource.Token);
                        if (result)
                        {
                            _logger.LogWarning(
                                $"Adding Ip to adapter collection and notifying. adapter={adapterName} ip={string.Join(", ", ipAddresses)}");
                            _ipAddresses[adapterName] = ipAddresses;
                        }
                    }
                }
            }

            foreach (var key in _ipAddresses.Keys.ToList().Where(key => !adapters.Contains(key)))
            {
                _logger.LogInformation($"Removing adapter from collection. adapter={key}");
                _ipAddresses.Remove(key);
                await _notificationService.Notify(new AdapterChange($"{key} - Removed", new List<string>()),
                    _cancellationTokenSource.Token);
            }
        }
    }

    private bool ListsMatch<T>(List<T>? list1, List<T> list2)
    {
        //here we check the count of list elements if they match, it can work also if the list count doesn't meet, to do it just comment out this if statement
        if (list1 == null)
        {
            return false;
        }

        if (list1.Count != list2.Count)
        {
            return false;
        }


        if (list1.Any(item => list2.Find(i => i.Equals(item)) == null))
        {
            return false;
        }

        return list2.All(item => list1.Find(i => i.Equals(item)) != null);
    }
}
