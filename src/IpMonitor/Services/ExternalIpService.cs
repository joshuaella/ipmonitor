using IpMonitor.Models;
using IpMonitor.Services.Interfaces;

namespace IpMonitor.Services;

public class ExternalIpService : IExternalIpService
{
    private readonly ITaskServiceFactory _taskServiceFactory;
    private readonly IRestCallService _restCallService;
    private readonly INotificationService _notificationService;
    private readonly ILogger<ExternalIpService> _logger;
    private string _ip = string.Empty;

    public ExternalIpService(
        ITaskServiceFactory taskServiceFactory,
        IRestCallService restCallService,
        INotificationService notificationService,
        ILogger<ExternalIpService> logger)
    {
        _taskServiceFactory = taskServiceFactory;
        _restCallService = restCallService;
        _notificationService = notificationService;
        _logger = logger;
    }

    public async Task Start(CancellationToken token)
    {
        _logger.LogInformation("Starting External Ip Monitor");
        _ip = string.Empty;
        while (!token.IsCancellationRequested)
        {
            await CheckNetworkForChanges(token);
            if (!token.IsCancellationRequested)
            {
                await _taskServiceFactory.Delay(TimeSpan.FromMinutes(5), token);
            }
        }

        _logger.LogInformation("Task Canceled and found in loop");
    }

    public void Stop()
    {
        _ip = string.Empty;
        _logger.LogInformation("Stopping External Ip Monitor");
    }

    private async Task CheckNetworkForChanges(CancellationToken token)
    {
        try
        {
            var result = await _restCallService.ExternalApi.GetExternalIp(token);
            if (result.IsSuccessStatusCode)
            {
                var newAddress = result.Content;
                if (newAddress != null && _ip != result.Content)
                {
                    var updated =
                        await _notificationService.Notify(
                            new AdapterChange("External Ip", new List<string> {newAddress}), token);
                    if (updated)
                    {
                        _ip = newAddress;
                    }
                }
            }
        }
        catch (OperationCanceledException e)
        {
            _logger.LogError(e, "CheckNetworkForChanges OperationCanceledException. message={message}", e.Message);
            if (e.CancellationToken == token)
            {
                _logger.LogWarning("CheckNetworkForChanges Canceled");
            }
        }
        catch (AggregateException ae)
        {
            _logger.LogError(ae, "CheckNetworkForChanges AggregateException. message={Message}", ae.Message);
            foreach (var inner in ae.Flatten().InnerExceptions)
            {
                _logger.LogError(inner, "Inner Exception message={InnerMessage} source={InnerSource}", inner.Message,
                    inner.Source);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Unhandled Exception message={Message} source={Source}", e.Message, e.Source);
        }
    }
}