﻿using IpMonitor.Services.Interfaces;
using Refit;

namespace IpMonitor.Services;

public class RestCallService : IRestCallService
{
    public RestCallService(
        string slackUrl,
        string externalIpUrl)
    {
        SlackApi = RestService.For<ISlackApi>(slackUrl);
        ExternalApi = RestService.For<IExternalIpApi>(externalIpUrl);
    }

    public ISlackApi SlackApi { get; }
    public IExternalIpApi ExternalApi { get; }
}