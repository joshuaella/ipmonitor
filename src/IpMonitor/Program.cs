using IpMonitor;
using IpMonitor.Models;
using IpMonitor.Services;
using IpMonitor.Services.Interfaces;
using NLog;
using NLog.Extensions.Logging;
using NLog.Extensions.Hosting;
using LogLevel = NLog.LogLevel;

var config = new ConfigurationBuilder().Build();
var logger = LogManager.Setup()
    .SetupExtensions(ext => ext.RegisterConfigSettings(config))
    .GetCurrentClassLogger();

try
{
    var host = Host.CreateDefaultBuilder(args)
        .ConfigureServices(services =>
        {
            services.AddSingleton<ITaskServiceFactory, TaskServiceFactory>();
            services.AddSingleton<IRestCallService>(s =>  
                new RestCallService(
                    StaticConfigurationManager.SlackUrl,
                    StaticConfigurationManager.ExternalIpUrl
                )
            );
            services.AddSingleton<ILocalIpService, LocalIpService>();
            services.AddSingleton<IExternalIpService, ExternalIpService>();
            services.AddSingleton<INotificationService>(s =>
                new NotificationService(
                    StaticConfigurationManager.SlackToken,
                    services.BuildServiceProvider().GetService<IRestCallService>()!,
                    services.BuildServiceProvider().GetService<ITaskServiceFactory>()!,
                    services.BuildServiceProvider().GetService<ILogger<NotificationService>>()!
                )
            );
            services.AddWindowsService(options =>
            {
                options.ServiceName = "IpMonitor";
            });
            services.AddHostedService<Worker>();
        })
        .ConfigureLogging(builder => builder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace))
        .UseNLog()
        .Build();

    host.Run();
}
catch (Exception ex)
{
    logger.Log(LogLevel.Error, $"Error Starting Service {ex.Message} {ex}");
}

