using System.Net.NetworkInformation;
using System.Text;

namespace IpMonitor.Models
{
    public class AdapterChange
    {
        private readonly string _hostname;

        public AdapterChange(string name, List<string> ips)
        {
            _hostname = GetFqdn();
            Name = name;
            IpAddresses = ips;
        }

        public string Name { get; }
        public List<string> IpAddresses { get; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(_hostname)
              .AppendLine()
              .Append($"Adapter Change: {Name}");
            foreach (var ip in IpAddresses)
            {
                sb.AppendLine()
                  .Append($"Ip Address: {ip}");
            }
            return sb.ToString();
        }

        private static string GetFqdn()
        {
            var properties = IPGlobalProperties.GetIPGlobalProperties();
            return $"{properties.HostName}{(string.IsNullOrEmpty(properties.DomainName) ? "" : $".{properties.DomainName}")}";
        }
    }
}