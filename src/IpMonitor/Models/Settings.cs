﻿namespace IpMonitor.Models
{
    public static class StaticConfigurationManager
    {
        public static IConfiguration AppSetting { get; }
        
        static StaticConfigurationManager()
        {
            AppSetting = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"{System.AppDomain.CurrentDomain.BaseDirectory}\\config.json")
                .Build();
        }

        public static string SlackUrl => AppSetting["slackUrl"] ?? string.Empty;
        public static string SlackToken => AppSetting["slackToken"] ?? string.Empty;
        public static string ExternalIpUrl => AppSetting["externalIpUrl"] ?? string.Empty;
    }
}