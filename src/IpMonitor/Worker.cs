using IpMonitor.Services.Interfaces;

namespace IpMonitor;

public class Worker : BackgroundService
{
    private readonly ILocalIpService _localIpService;
    private readonly IExternalIpService _externalIpService;
    private readonly ILogger<Worker> _logger;

    public Worker(
        ILocalIpService localIpService,
        IExternalIpService externalIpService,
        ILogger<Worker> logger)
    {
        _localIpService = localIpService;
        _externalIpService = externalIpService;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        var errored = false;
        try
        {
            _logger.LogInformation("Starting");
            _localIpService.Start();
            await _externalIpService.Start(cancellationToken);
        }
        catch (TaskCanceledException tce)
        {
            _logger.LogWarning(tce, "{Message}", tce.Message);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "{Message}", ex.Message);
            errored = true;
        }
        finally
        {
            _logger.LogInformation("Stopping");
            _localIpService.Stop();
            _externalIpService.Stop();
        }

        if (errored)
        {
            Environment.Exit(1);
        }
    }
}

