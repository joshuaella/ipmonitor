@echo off

echo root directory is "%~dp0"

: {WindowsServiceName} = Unique identifier of the Windows Service. // e.g. WorkerServiceExample
: {WindowsServiceName} = A custom name for our Windows Service
: {FullPathToExeFile} = The full path to where the executable (.exe) file of our published Worker Service project
: {DisplayName} = A friendly name for our Windows Service // e.g. Worker Service Example 
set WindowsServiceName=IPMonitor
set FullPathToExeFile=%~dp0..\..\out\IpMonitor\IpMonitor.exe
set Start=delayed-auto

echo %WindowsServiceName%
echo %FullPathToExeFile%
echo %Start%

icacls %~dp0..\..\out\IpMonitor /grant:r SYSTEM:(OI)(CI)(M)

sc delete %WindowsServiceName%
sc create %WindowsServiceName% binpath=%FullPathToExeFile% start=%Start% displayname="IPMonitor JoshuaElla"
sc description %WindowsServiceName% "Monitors IP Address changes and publishes to Slack"
sc start %WindowsServiceName%

